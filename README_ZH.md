# ℂ𝕝𝕒𝕤𝕤𝕪𝕊𝕆ℝ𝕋

ClassySORT是一个简单的实时多对象跟踪器（MOT），适用于任何类型的对象类（不仅仅是人）。
这是一个人的自由时间项目。我将尝试对问题做出回应，但无法做出任何支持承诺。在开源社区中，“请”和“谢谢”有很长的路要走。

![demo-footage](assets/velon-2019-creds.gif)

## Introduction

ClassySORT是一款最先进的（SOTA）多对象跟踪器（MOT），可用于您自己的项目。因为You-only look once算法（YOLO）检测器是在COCO数据集上预先训练的，ClassySORT可以“开箱即用(out of the box)”地检测、计数和跟踪80种不同的常见对象。
我的开发平台是Pop_OS！20.04（Ubuntu的分支）和NVIDIA RTX2070S。
修改它与使用自己的数据集训练YOLO完全相同。 [How do I do that?](https://github.com/ultralytics/yolov5/wiki/Train-Custom-Data)

**ClassySORT implements** 

+ [ultralytics/YOLOv5](https://github.com/ultralytics/yolov5/wiki) 无修改
+ [abewley/SORT](https://github.com/abewley/sort) 稍加修改

此存储库使用固定版本的YOLOv5以确保兼容性。将YOLOv5代码替换为更新的ultralytics/YOLOv5代码可能会导致中断更改。如果您能够解决此问题，请提交拉取请求。

如果您只需要跟踪人员，或者有资源使用自己的数据集从头开始训练模型，请参阅下面的“更复杂的MOT(More Complex MOTs)”部分。

## Using ClassySORT

克隆分支

```bash
git clone https://github.com/tensorturtle/classy-sort-yolov5.git
cd classy-sort-yolov5
```

### Install Requirements

Python 3.8 or later with all requirements.txt. To install run:

Python3.8或更高版本以及 `requirements.txt` 文件中要求的。要安装或运行：

```bash
pip install -r requirements.txt
```

### Download YOLOv5 weights

```bash
./download_weights.sh
```
此脚本将yolov5权重保存到`yolov5/weights/`目录。

### Run Tracking

要在自己的视频上运行跟踪器并查看跟踪的边界框，请运行： 

```bash
python classy_track.py --source /path/to/video.mp4 --view-img
```

要获取参数摘要，请运行：

```bash
python classy_track.py -h
```

文本结果按以下格式从上面的数组保存到 `/inference/output/` 。脚本中的这个位置也是将您自己的程序插入其中的一个好位置。
保存的文本文件包含以下信息：

```bash
[frame_index, x_left_top, y_left_top, x_right_bottom, y_right_bottom, object_category, u_dot, v_dot, s_dot, object_id]
```

其中

+ u_dot: x_中心的时间导数（像素）
+ v_dot: y_中心的时间导数（像素）
+ s_dot: 标度（bbox面积）的时间导数，以像素为单位

```python
np.concatenate((convert_x_to_bbox(self.kf.x), arr_detclass, arr_u_dot, arr_v_dot, arr_s_dot), axis=1)
""" 
:param: convert_x_to_bbox(self.kf.x): bbox
:param: arr_detclass: 类别
:param: arr_u_dot: x_中心的时间导数（像素）
:param: arr_v_dot: y_中心的时间导数（像素）
:param: arr_s_dot: 标度（bbox面积）的时间导数，以像素为单位
"""
```



## 实现细节

### 针对 SORT 的修改

#### 1. 类别感知跟踪

SORT的原始实现丢弃了YOLO的对象类信息（0:person、1:bike 等）。
我想保留这些信息，所以我在 `sort.py` 中的 `KalmanBoxTracker` 对象中添加了 `detclass` 属性。:

![modifications_to_sort_schematic](assets/sort-mod.png)

#### 2.卡尔曼滤波参数

我发现，对于我自己的数据集，边界框的大小变化相当快，默认的Q值（过程协方差）太低。我建议你试着用它们做实验。


## More Complex MOTs
如果您只需要跟踪人员，或者有资源使用自己的数据集从头开始训练模型，那么我推荐 [bostom/Yolov5_DeepSort_PyTorch](https://github.com/mikel-brostrom/Yolov5_DeepSort_Pytorch).

DeepSORT在SORT的基础上增加了一个单独训练的神经网络，这增加了人类检测的准确性，但略微降低了性能。

It also means that using your custom dataset involves training both YOLO and DeepSORT's 'deep association metric'

这还意味着使用自定义数据集需要训练YOLO和DeepSORT的“深度关联度量”

For a 'bag of tricks' optimized version of YOLOv5 + DeepSORT, see [GeekAlexis/FastMOT](https://github.com/GeekAlexis/FastMOT)

有关YOLOv5+DeepSORT的“技巧包”优化版本，请参阅[GeekAlexis/FastMOT](https://github.com/GeekAlexis/FastMOT)

## License

ClassySORT是在GPL许可证版本3下发布的，旨在促进追踪器的开放使用和未来的改进。
这意味着此存储库中的代码不能用于封闭源代码发行版，您还必须将任何派生代码作为GPL3进行许可。

