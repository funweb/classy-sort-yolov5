import os
import shutil

base_dir = r"F:\classy-sort-yolov5\inference\datasets\imgs"

file_names = os.listdir(base_dir)

for old_file_name in file_names:
    old_file_path = os.path.join(base_dir, old_file_name)

    (filename, extension) = os.path.splitext(old_file_name)   # 文件名 和
    frames_name_indexs = filename.split("_")
    frame_name = frames_name_indexs[0]
    frame_index = frames_name_indexs[1].zfill(5)  # 补足5位

    new_file_name = "%s_%s%s" % (frame_name, frame_index, extension)

    new_file_path = os.path.join(base_dir, new_file_name)

    shutil.move(old_file_path, new_file_path)

